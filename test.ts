import { expect } from 'chai'
import { RandomPicture } from './index'

describe('RandomImage', () => {
  it('test if all needed keys are present', async () => {
    const neededKeys = ['url']
    const random = await RandomPicture()
    expect(Object.keys(random)).to.include.members(neededKeys)
  })
})
