interface ImageObject {
  url: string;
  author?: string;
  width?: number;
  height?: number;
}

const PROVIDERS = [
  'picsum-photos',
]

async function RandomPicture (): Promise<ImageObject> {
  // Get random provider
  const provider = PROVIDERS[Math.floor(Math.random() * PROVIDERS.length)]

  if (provider === 'picsum-photos') {
    const { Picsum } = await import('picsum-photos')
    const image = await Picsum.random()
    return {
      url: image.download_url,
      author: image.author,
      width: image.width,
      height: image.height,
    }
  }
  throw new Error('Image provider not implemented: ' + provider)
}

export { RandomPicture }
